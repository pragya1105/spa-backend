const {UserModel} = require('../Models/userModel')
const jwt = require('jsonwebtoken');
const Joi = require('joi');

/*-------------------------------
+++++++++++++ Social signup ++++++++++++
--------------------------------*/

exports.CompleteSignup = async(req, res) => {
	    
	try {

		let {socail_id} = req.body
		console.log("req.body==============", req.body)
		    const schema = Joi.object().keys({
				socail_id :Joi.string().required(),
				socail_type:Joi.string().optional().allow(''),
				mobileNumber :Joi.string().optional().allow(''),
				email: Joi.string().optional().allow(''),
				name : Joi.string().optional().allow(''),
				profileImage : Joi.string().optional().allow(''),
        	})
        	const result = Joi.validate(req.body, schema, { abortEarly: true });
	        	if (result.error) {
		            if (result.error.details && result.error.details[0].message) {
		                responses.parameterMissing(res, result.error.details[0].message);
		                return;
		            } else {
		                responses.parameterMissing(res, result.error.message);
		                return;
	            }
			}

			req.body.is_signup_complete = '1'
			
			let updateUser = await UserModel.findOneAndUpdate({socail_id:socail_id},req.body,{new : true})

			let token = jwt.sign({ id : updateUser._id }, 'supersecret' );
			updateUser.access_token = token

        	res.status(200).send({ response : updateUser, message: 'Logged In Successfully' });
           
		} catch(error) {
			res.status(403).send({ message: error });
		}
}


exports.isSocailUserExist = async(req, res) => {
	    
	try {
			const schema = Joi.object().keys({
				socail_id :Joi.string().required(),
				socail_type:Joi.string().optional().allow(''),
				mobileNumber :Joi.string().optional().allow(''),
				email: Joi.string().optional().allow(''),
				name : Joi.string().optional().allow(''),
				profileImage : Joi.string().optional().allow(''),
        	})
        	const result = Joi.validate(req.body, schema, { abortEarly: true });
	        	if (result.error) {
		            if (result.error.details && result.error.details[0].message) {
		                responses.parameterMissing(res, result.error.details[0].message);
		                return;
		            } else {
		                responses.parameterMissing(res, result.error.message);
		                return;
	            }
			}

			let loggedInUser = await UserModel.findOne({
				socail_id : req.body.socail_id
			}).lean(true)

			if(!loggedInUser)
				  loggedInUser =   await UserModel.create(req.body)
				  

				  if(loggedInUser.is_signup_complete == '1')
						  loggedInUser.access_token = jwt.sign({ id : loggedInUser._id }, 'supersecret' );

			res.status(200).send({ response : loggedInUser, message: 'Exists' });
           
		} catch(error) {
			res.status(403).send({ message: error });
		}
}

/*-------------------------------
+++++++++++++ Get User Profile ++++++++++++
--------------------------------*/

exports.getUserProfile = async(req, res) => {
	    
	try {
			let loggedInUser = await UserModel.findById(req.userId)

			res.status(200).send({ response : loggedInUser, message: 'JWT has been expired' });
           
		} catch(error) {
			res.status(403).send({ message: error });
		}
}
