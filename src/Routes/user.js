var user = require ('../Controllers/userController');
var auth = require ('../Modules/auth');

exports.getRouter = (app) => {

    app.route("/user/CompleteSignup").post(user.CompleteSignup);
    
    app.route("/user/isSocailUserExist").post(user.isSocailUserExist)
    app.route("/user/getUserProfile").get(auth.verifyToken, user.getUserProfile)
    
}
