var express = require ("express");
var cors = require ("cors");
var path = require ("path");
var glob = require ("glob");
var bodyParser = require ("body-parser");

const app = express();
app.use(bodyParser.json());
app.use(cors());
require('./Routes/user').getRouter(app); 

app.use(express.static(path.join(__dirname, '..','dist','build')));
app.get('**', (req, res) => {
	res.sendFile(path.resolve(path.join(__dirname,'..','dist','build','index.html')));
});

app.use(bodyParser.urlencoded({
    extended: true
}));
const port = process.env.PORT || 3000;

app.listen(port);
console.log("Started on port " + port);